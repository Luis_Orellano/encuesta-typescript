import { ArrayList } from "./tools/ArrayList.js";
import { Encuesta } from "./Encuensta.js";

class Respuesta {
  private encuesta: Encuesta;
  private respuestas: ArrayList<string>;
  private edad?: number;

  constructor() {
    this.encuesta = new Encuesta();
    this.respuestas = new ArrayList<string>();
  }

  public addRespuesta(respuesta: string) {
    this.respuestas.add(respuesta);
  }

  public getRespuestas(): string[] {
    return this.respuestas.getAll();
  }

  public setEdad(edad: number) {
    this.edad = edad;
  }

  public getEdad(): number | undefined {
    return this.edad;
  }

  public removeRespuestas() {
    this.respuestas.remove();
  }

  public calcularEdadPersona(fecha: string): number {
    let splitFecha: string[] = fecha.split("-");
    let date: Date = new Date(
      parseInt(splitFecha[0]),
      parseInt(splitFecha[1]),
      parseInt(splitFecha[2])
    );
    let yearNow = new Date().getFullYear();
    let monthNow = new Date().getMonth();
    let edad: number;

    if (date.getMonth() > monthNow) {
      edad = yearNow - date.getFullYear() - 1;
    } else {
      edad = yearNow - date.getFullYear();
    }

    return edad;
  }

  public contar(lista: string[]): number[] {
    const resultSI: number = lista.filter((lista) => lista == "SI").length;
    const resultNO: number = lista.filter((lista) => lista == "NO").length;

    return [resultSI, resultNO];
  }
}
export { Respuesta };

class RespuestaContable {
  private si: number;
  private no: number;

  constructor() {
    this.si = 0;
    this.no = 0;
  }

  getSi() {
    return this.si;
  }

  getNo() {
    return this.no;
  }

  setSi(si: number) {
    this.si = si;
  }

  setNo(no: number) {
    this.no = no;
  }
}
export { RespuestaContable };
