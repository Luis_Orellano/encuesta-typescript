export class ArrayList<T>{
    private items:T[];

    constructor(){
        this.items = [];
    }

    add(item:T):void{
        this.items.push(item);
    }

    get(index:number):T|null{
        const item:T[] = this.items.filter((x:T, i:number) => {
            return i === index;
        });

        if(item.length === 0){
            return null;
        } else {
            return item[0];
        }
    }

    createFrom(value:T[]){
        this.items = [...value];
    }

    getAll():T[]{
        return this.items;
    }

    size():number{
        return this.items.length - 1;
    }

    remove():void{
        this.items.splice(0);
    }
}