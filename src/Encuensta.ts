import { ArrayList } from "./tools/ArrayList.js";

class Encuesta {

    private preguntas: ArrayList<string>;

    constructor() {
        this.preguntas = new ArrayList<string>();
    }

    public ingresarPregunta(pregunta: string) {
        this.preguntas.add(pregunta);
    }

    public getPregunta(index: number): string | null {
        return this.preguntas.get(index);
    }

    public getAllPreguntas(): string[] {
        return this.preguntas.getAll();
    }

    public getLastIndex(): number {
        return this.preguntas.size();
    }

} export { Encuesta };