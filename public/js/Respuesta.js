import { ArrayList } from "./tools/ArrayList.js";
import { Encuesta } from "./Encuensta.js";
class Respuesta {
    constructor() {
        this.encuesta = new Encuesta();
        this.respuestas = new ArrayList();
    }
    addRespuesta(respuesta) {
        this.respuestas.add(respuesta);
    }
    getRespuestas() {
        return this.respuestas.getAll();
    }
    setEdad(edad) {
        this.edad = edad;
    }
    getEdad() {
        return this.edad;
    }
    removeRespuestas() {
        this.respuestas.remove();
    }
    calcularEdadPersona(fecha) {
        let splitFecha = fecha.split("-");
        let date = new Date(parseInt(splitFecha[0]), parseInt(splitFecha[1]), parseInt(splitFecha[2]));
        let yearNow = new Date().getFullYear();
        let monthNow = new Date().getMonth();
        let edad;
        if (date.getMonth() > monthNow) {
            edad = yearNow - date.getFullYear() - 1;
        }
        else {
            edad = yearNow - date.getFullYear();
        }
        return edad;
    }
    contar(lista) {
        const resultSI = lista.filter((lista) => lista == "SI").length;
        const resultNO = lista.filter((lista) => lista == "NO").length;
        return [resultSI, resultNO];
    }
}
export { Respuesta };
class RespuestaContable {
    constructor() {
        this.si = 0;
        this.no = 0;
    }
    getSi() {
        return this.si;
    }
    getNo() {
        return this.no;
    }
    setSi(si) {
        this.si = si;
    }
    setNo(no) {
        this.no = no;
    }
}
export { RespuestaContable };
