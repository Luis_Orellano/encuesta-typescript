import { RespuestaContable } from "./Respuesta.js";
const respAdolescentes = new RespuestaContable();
const respJovenes = new RespuestaContable();
const respAdultos = new RespuestaContable();
const respAdultosMayores = new RespuestaContable();
class UI {
    renderPreguntas(encuesta) {
        let html = "";
        let element = this.$(".contenido-preguntas");
        let lastIndex = encuesta.getLastIndex();
        const pregunta = encuesta.getPregunta(lastIndex);
        html += `
          <div class="text-center form-group" id="div-pregunta"><b>${pregunta}</b>
            
                SI: <input type="radio" id="respuesta-${lastIndex}" name="respuesta-${lastIndex}" value="SI" required>
                NO: <input type="radio" id="respuesta-${lastIndex}" name="respuesta-${lastIndex}" value="NO" required>
            
          </div>
          `;
        element.insertAdjacentHTML("afterbegin", html);
        document.querySelector("#input_pregunta").value = "";
    }
    renderEdad(edad) {
        let html = "";
        let element = this.$(".contenido-edad");
        while (element.hasChildNodes()) {
            element.removeChild(element.lastChild);
        }
        html += `
    <div class="text-center" id="div-edad"><b></b>
      <span class="span-text-edad"> Su edad es: ${edad}</span>
    </div>
    `;
        element.insertAdjacentHTML("afterbegin", html);
    }
    renderEstadisticas(encuesta, respuesta) {
        let html = "";
        let element = this.$(".contenido-resultados");
        let lastIndex = encuesta.getLastIndex();
        encuesta.getAllPreguntas().forEach((item) => {
            const resp = document.querySelector(`#respuesta-${lastIndex}`);
            if (resp.checked == false) {
                respuesta.addRespuesta("NO");
            }
            else {
                respuesta.addRespuesta("SI");
            }
            lastIndex--;
        });
        console.log(respuesta.getRespuestas());
        const resp = respuesta.contar(respuesta.getRespuestas());
        if (respuesta.getEdad() >= 14 && respuesta.getEdad() <= 18) {
            this.contarStats(respAdolescentes, resp);
        }
        else if (respuesta.getEdad() >= 19 && respuesta.getEdad() <= 29) {
            this.contarStats(respJovenes, resp);
        }
        else if (respuesta.getEdad() >= 30 && respuesta.getEdad() <= 64) {
            this.contarStats(respAdultos, resp);
        }
        else if (respuesta.getEdad() >= 65) {
            this.contarStats(respAdultosMayores, resp);
        }
        else {
            return alert("fuera del rango de edades permitidos");
        }
        while (element.hasChildNodes()) {
            element.removeChild(element.lastChild);
        }
        html += `
        <div class="text-center container card">
          <p> ADOLESCENTES: SI=${respAdolescentes.getSi()} / NO=${respAdolescentes.getNo()}</p>
          <p> JÓVENES: SI=${respJovenes.getSi()} / NO=${respJovenes.getNo()}</p>
          <p> ADULTOS: SI=${respAdultos.getSi()} / NO=${respAdultos.getNo()}</p>
          <p> ADULTOS MAYORES: SI=${respAdultosMayores.getSi()} / NO=${respAdultosMayores.getNo()}</p>
        </div>
        `;
        element.insertAdjacentHTML("afterbegin", html);
        document.querySelector("#input-nacimiento").value =
            "";
        this.$("#div-edad").hidden = true;
        resp.splice(0);
        this.$(".span-text-edad").hidden = false;
        let index = encuesta.getLastIndex();
        encuesta.getAllPreguntas().forEach(() => {
            let allPreguntas = document.querySelectorAll(`#respuesta-${index}`);
            allPreguntas.forEach((x) => (x.checked = false));
            index--;
        });
    }
    contarStats(respCont, resp) {
        const totalSi = respCont.getSi() + resp[0];
        const totalNo = respCont.getNo() + resp[1];
        respCont.setSi(totalSi);
        respCont.setNo(totalNo);
    }
    $(selector) {
        return document.querySelector(selector);
    }
    $$(selector) {
        return document.querySelectorAll(selector);
    }
}
export { UI };
