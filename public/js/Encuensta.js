import { ArrayList } from "./tools/ArrayList.js";
class Encuesta {
    constructor() {
        this.preguntas = new ArrayList();
    }
    ingresarPregunta(pregunta) {
        this.preguntas.add(pregunta);
    }
    getPregunta(index) {
        return this.preguntas.get(index);
    }
    getAllPreguntas() {
        return this.preguntas.getAll();
    }
    getLastIndex() {
        return this.preguntas.size();
    }
}
export { Encuesta };
