import { Encuesta } from "./Encuensta.js";
import { Respuesta } from "./Respuesta.js";
import { UI } from "./UI.js";
const btn_pregunta = document.querySelector("#btn_pregunta");
const form_respuestas = document.querySelector("#cont-preg");
const btn_edad = document.querySelector("#btn_nacimiento");
const input_edad = document.querySelector("#input-nacimiento");
const input_pregunta = document.querySelector("#input_pregunta");
const btn_init = document.querySelector("#btn_init");
const encuesta = new Encuesta();
const respuesta = new Respuesta();
const ui = new UI();
//Eventos con el DOM
btn_init === null || btn_init === void 0 ? void 0 : btn_init.addEventListener("click", (e) => {
    let element = ui.$(".contenido-preguntas");
    if (element.hasChildNodes()) {
        ui.$(".span-pregunta").hidden = true;
        input_pregunta.hidden = true;
        ui.$("#btn_pregunta").hidden = true;
        ui.$("#btn_init").hidden = true;
        input_edad.hidden = false;
        ui.$("#btn_nacimiento").hidden = false;
        ui.$("#btn_respuesta").hidden = false;
    }
    else {
        alert("No puede iniciar el cuestionario sin tener preguntas para responder");
    }
});
btn_pregunta === null || btn_pregunta === void 0 ? void 0 : btn_pregunta.addEventListener("click", (e) => {
    //const ui = new UI();
    if (input_pregunta.value != "") {
        const pregunta = input_pregunta.value;
        encuesta.ingresarPregunta(pregunta);
        ui.renderPreguntas(encuesta);
    }
    else {
        alert("completa los datos");
    }
});
btn_edad === null || btn_edad === void 0 ? void 0 : btn_edad.addEventListener("click", (e) => {
    //const ui = new UI();
    if (input_edad.value != "") {
        const fecha = input_edad.value;
        let edad = respuesta.calcularEdadPersona(fecha);
        respuesta.setEdad(edad);
        respuesta.removeRespuestas();
        ui.renderEdad(respuesta.getEdad());
    }
    else {
        alert("Ingrese su fecha de nacimiento por favor");
    }
});
form_respuestas === null || form_respuestas === void 0 ? void 0 : form_respuestas.addEventListener('submit', (e) => {
    let element = ui.$(".contenido-preguntas");
    if (element.hasChildNodes() && input_edad.value != "") {
        ui.renderEstadisticas(encuesta, respuesta);
    }
    else if (!element.hasChildNodes()) {
        alert("NO existen preguntas");
    }
    else if (input_edad.value == "") {
        alert("No se registro una fecha de nacimiento");
    }
    e.preventDefault();
});
